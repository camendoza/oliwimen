import model.data_structures.ArbolRojoNegro;
import model.data_structures.NodoRojoNegro;
import junit.framework.TestCase;


public class ArbolRojoNegroTest extends TestCase{
	
	private ArbolRojoNegro<String, Integer> arbol;
	
	public void setupEscenario1()
	{
		arbol = new ArbolRojoNegro<>();
		
	}
	public void setupEscenario2()
	{
		arbol = new ArbolRojoNegro<>();
		arbol.put("a", 1);
		arbol.put("b", 2);
		arbol.put("c", 3);
		arbol.put("d", 4);
	}
	
	public void setupEscenario3()
	{
		arbol = new ArbolRojoNegro<>();
		arbol.put("5", 1);
		arbol.put("4", 1);
		arbol.put("3", 1);
		
	}
	public void testGetValue()
	{
		setupEscenario2();
		assertEquals("El valor no es el esperado",Integer.valueOf(1),arbol.getNodo("a").val);
		assertEquals("El valor no es el esperado",Integer.valueOf(2),arbol.getNodo("b").val);
		assertEquals("El valor no es el esperado",Integer.valueOf(3),arbol.getNodo("c").val);
		assertEquals("El valor no es el esperado",Integer.valueOf(4),arbol.getNodo("d").val);
		
	}
	
	public void testPut()
	{
		setupEscenario1();
		assertEquals("El arbol deberia estar vacio",0,arbol.size());
		arbol.put("primero", 1);
		assertEquals("El tama�o del arbol no coincide",1,arbol.size());
		arbol.put("segundo", 2);
		arbol.put("tercero", 3);
		assertEquals("El tama�o del arbol no coincide",3,arbol.size());
	}
	
	public void testFlipColors()
	{   arbol = new ArbolRojoNegro<>();
	    NodoRojoNegro<String, Integer> nodo = new NodoRojoNegro<String, Integer>("a",1,1,true);
	    arbol.flipColors(nodo);
	    assertFalse("El color no es el esperado",nodo.isRed(nodo));
	    arbol.flipColors(nodo);
	    assertTrue("El color no es el esperado",nodo.isRed(nodo));
	   
	 }
	
	
	public void testGetNodo()
	{
		setupEscenario2();
		
		assertEquals("El nodo no es el esperado",Integer.valueOf(1),arbol.getNodo("a").val);
		assertEquals("El nodo no es el esperado",Integer.valueOf(2),arbol.getNodo("b").val);
		assertEquals("El nodo no es el esperado",Integer.valueOf(3),arbol.getNodo("c").val);
		assertEquals("El nodo no es el esperado",Integer.valueOf(4),arbol.getNodo("d").val);
		
	}
	
	public void testSize()
	{
		setupEscenario2();
		assertEquals("El arbol deberia estar vacio",4,arbol.size());
		arbol.put("primero", 1);
		assertEquals("El tama�o del arbol no coincide",5,arbol.size());
		arbol.put("segundo", 2);
		arbol.put("tercero", 3);
		assertEquals("El tama�o del arbol no coincide",7,arbol.size());
	}
	
	

}
