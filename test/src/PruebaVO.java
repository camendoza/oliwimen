

public class PruebaVO implements Comparable<PruebaVO>{
	String nombre;
	Integer agno;

	public void setAgno(int agno)
	{
		this.agno=agno;
	}
	public int getAgno()
	{
		return agno;
	}
	public void setNombre(String nombre)
	{
		this.nombre=nombre;
	}
	public String getNombre()
	{
		return nombre;
	}
	@Override
	public int compareTo(PruebaVO prueba) {
		int aux = agno - prueba.getAgno();
		if (aux > 0)
			return 1;
		else if (aux < 0)
			return -1;
		else
		{
			aux = nombre.compareTo(prueba.getNombre());
			if (aux > 0)
				return 1;
			else if(aux < 0)
				return -1;
			else
				return 0;
		}
	}
}
