

import junit.framework.TestCase;
import model.data_structures.MaxHeapCP;
import model.logic.GeneradorDatos;
import model.vo.PruebaVO;

public class MaxHeapCPTest extends TestCase
{
	private MaxHeapCP<PruebaVO> heap;
	private PruebaVO mayor;
	/**
	 * Constante para cambiar el tama�o de las muestras. En este caso, 2^15.
	 */
	private final static int tamanio = 32768;
	public void setupEscenario1()
	{
		heap = new MaxHeapCP<PruebaVO>();
	}
	public void setupEscenario2()
	{
		long start = System.currentTimeMillis();
		setupEscenario1();
		PruebaVO newMayor = new PruebaVO();
		newMayor.setAgno(0);
		newMayor.setNombre("");
		mayor = newMayor;
		heap.crearCP(tamanio);
		String[] muestrasCadenas = GeneradorDatos.generarCadenas(tamanio);
		Integer[] muestrasEnteros = GeneradorDatos.generarAgnos(tamanio);
		PruebaVO aux = new PruebaVO();
		for(int i = 0; i < tamanio; i++)
		{
			aux.setNombre(muestrasCadenas[i]);
			aux.setAgno(muestrasEnteros[i]);
			if(aux.compareTo(mayor)>0)
				mayor = aux;
			try {
				heap.agregar(aux);
			} catch (Exception e) 
			{
				System.out.println(e.getMessage());
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("El tiempo transcurrido para agregar " + tamanio + " elementos es de " 
				+ (end-start) + " milisegundo/s");
	}
	public void setupEscenario3()
	{
		setupEscenario1();
		heap.crearCP(40);
		String[] muestrasCadenas = GeneradorDatos.generarCadenas(39);
		Integer[] muestrasEnteros = GeneradorDatos.generarAgnos(39);
		PruebaVO aux = new PruebaVO();
		for(int i = 0; i < 39; i++)
		{
			aux.setNombre(muestrasCadenas[i]);
			aux.setAgno(muestrasEnteros[i]);
			try {
				heap.agregar(aux);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void testMax()
	{
		setupEscenario1();
		assertNull(heap.max());
		setupEscenario2();
		assertEquals(tamanio, heap.darNumeroElementos());
		long start = System.currentTimeMillis();
		PruebaVO aux = heap.max();
		assertEquals(mayor.getAgno(), aux.getAgno());
		assertEquals(mayor.getNombre(), aux.getNombre());
		long end = System.currentTimeMillis();
		System.out.println("El tiempo transcurrido para sacar el m�ximo elemento de una cola con " 
				+ tamanio + " elementos es de " + (end-start) + " milisegundo/s");

	}
	public void testAgregar()
	{
		setupEscenario3();
		PruebaVO aux = new PruebaVO();
		aux.setNombre("Nombre");
		aux.setAgno(2010);
		try {
			heap.agregar(aux);
		} catch (Exception e) {
			assert false;
		}
		try {
			heap.agregar(aux);
		} catch (Exception e) {
			assert true;
		}
	}
}
