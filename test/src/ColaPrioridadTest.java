
import model.data_structures.ColaPrioridad;
import junit.framework.TestCase;


public class ColaPrioridadTest extends TestCase{
	
	private ColaPrioridad<PruebaVO> cola;	
	public void setupEscenario1()
	{
		cola = new ColaPrioridad<PruebaVO>();
	}
	
	public void setupEscenario2() 
	{   System.out.println("pruebaaa");
		cola = new ColaPrioridad<PruebaVO>();
		System.out.println("pruebaaa");
		cola.crearCP(4);
		System.out.println("pruebaaa 2");
		PruebaVO nuevo = new PruebaVO();
		nuevo.setAgno(2000);
		nuevo.setNombre("Nombre 1");
		try 
		{
			cola.agregar(nuevo);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public void setupEscenario3() 
	{   
		cola = new ColaPrioridad<PruebaVO>();
		cola.crearCP(3);
		PruebaVO nuevo = new PruebaVO();
		nuevo.setAgno(2000);
		nuevo.setNombre("Nombre 1");
		PruebaVO nuevo2 = new PruebaVO();
		nuevo2.setAgno(2010);
		nuevo2.setNombre("Nombre 2");
		try 
		{
			cola.agregar(nuevo);
			cola.agregar(nuevo2);
		} 
		catch (Exception e) 
		{
			e.getMessage();
		}
	}
	
	public void testAgregar()
	{
		System.out.println("antes");
		setupEscenario2();
		assertEquals( "El elemento no se agrego", 1, cola.darNumElementos() );
		assertFalse( "La cola esta vacia", cola.esVacia() );
		
	}
	
	public void testMax()
	{
		setupEscenario3();
		assertEquals( "No se agregaron correctamente los elemnetos", 2, cola.darNumElementos() );
		assertEquals( "El tamanio max no corresponde", 3, cola.tamanoMax() );
		assertEquals( "El elemento max no corresponde","Nombre 2" , cola.max().getNombre());
		assertEquals( "El numero de elementos no corresponde", 1, cola.darNumElementos());
	}
	
	

}
