import junit.framework.TestCase;
import model.data_structures.BST;

public class BSTTest extends TestCase{
	
	private BST<String, Integer> arbol;
	public void setupEscenario1()
	{
		arbol = new BST<String, Integer>();
	}
	
	public void setupEscenario2()
	{
		arbol = new BST<String, Integer>();
		arbol.put("b", 5);
		arbol.put("c", 6);
		arbol.put("a", 3);
	}
	public void setupEscenario3()
	{
		arbol = new BST<String, Integer>();
		arbol.put("b", 5);
		arbol.put("c", 6);
		arbol.put("a", 3);
		arbol.put("z", 19);
		arbol.put("m", 15);
	}
	
	public void testPut()
	{
		setupEscenario2();
		assertEquals("El tama�o del arbol no corresponde",3,arbol.size());
		assertEquals("El maximo no coincide","c",arbol.max());
		assertEquals("El minimo no coincide","a",arbol.min());
	}
	
	public void testDelete()
	{
		setupEscenario2();
		arbol.deleteMin();
		assertEquals("El tama�o del arbol no corresponde",2,arbol.size());
		assertNull("No deberia encontrar el elemento",arbol.get("a"));
		assertNotNull("Deberia encontrar el elemento",arbol.get("b"));
		assertNotNull("Deberia encontrar el elemento",arbol.get("c"));
		
	}
	
	public void testValue()
	{
		setupEscenario2();
		assertEquals("El tama�o del arbol no corresponde",3,arbol.get("a").intValue());
		assertEquals("El tama�o del arbol no corresponde",5,arbol.get("b").intValue());
		assertEquals("El tama�o del arbol no corresponde",6,arbol.get("c").intValue());
	}
	
	
	
	
	

}
