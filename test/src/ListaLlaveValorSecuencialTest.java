import model.data_structures.ListaLlaveValorSecuencial;
import junit.framework.TestCase;

public class ListaLlaveValorSecuencialTest extends TestCase {
	private ListaLlaveValorSecuencial<String, Integer> lvs;
	public void setupEscenario1()
	{
		lvs = new ListaLlaveValorSecuencial<String, Integer>();
	}
	
	
	
	public void testInsertar()
	{
		setupEscenario1();
		lvs.insertar("llave1", 1);
		lvs.insertar("llave2", 2);
		lvs.insertar("llave3", 3);
		assertFalse("La lista no deberia estar vacia",lvs.estaVacia());
		assertTrue("No encontro la llave",lvs.tieneLlave("llave1"));
		assertTrue("No encontro la llave 2 ",lvs.tieneLlave("llave2"));
		assertFalse("No deberia encontrar la llave",lvs.tieneLlave("llave22"));
	}
	
	

}
