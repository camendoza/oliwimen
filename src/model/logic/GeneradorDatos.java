package model.logic;

import java.util.concurrent.ThreadLocalRandom;

public class GeneradorDatos 
{
	private final static char[] caracteres = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','�','o',
		'p','q','r','s','t','u','v','w','x','y','z'};
	private final static int TAMANIO_POR_CADENA = 15;
	
	public static String[] generarCadenas(int N)
	{
		String[] toReturn = new String[N];
		for(int j = 0; j < N; j++)
		{
			String aux = "";
			for ( int i = 0; i < TAMANIO_POR_CADENA; i++ ){
				int a = (int)(Math.random()*caracteres.length);
				aux += caracteres[a];
			}
			toReturn[j] = aux;
		}
		return toReturn;
	}
	
	public static Integer[] generarAgnos(int N)
	{
		Integer[] toReturn = new Integer[N];
		for( int i = 0; i < N; i++ )
		{
			int aux = ThreadLocalRandom.current().nextInt(1950, 2017);
			toReturn[i] = aux;
		}
		return toReturn;
	}
}
