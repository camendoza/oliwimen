package model.logic;

import java.util.Comparator;

import model.data_structures.ILista;

public class ShellSort {
	
	public static <T>void Sort(ILista<T> c, Comparator<T> k)
	{
		int N = c.darNumeroElementos();
		int h=1;
		while(h<N/3)
		{
			h= 3*h +1;
		}
		while(h>=1)
		{
			for(int i=h;i<N; i++)
			{
				for(int j=i; j>=h && less(c.darElemento(j), c.darElemento(j-h), k);j-=h)
				{
					c.cambiarElemento(j, j-h);
				}
			}
			h = h/3;
		}
	}

	private static boolean less(Object comparable, Object comparable2, Comparator k) {
		return k.compare(comparable, comparable2)<0;
	}

}
