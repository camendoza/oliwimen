package model.logic;

import java.util.Comparator;

import model.vo.VOPelicula;

public class OrdenarPeliculasReq11 implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula peli1, VOPelicula peli2) {
		// TODO Auto-generated method stub
		

		
		if (peli1.getAnio() == peli2.getAnio()) {
			if (peli1.getPaises().darElemento(0).equals(peli2.getPaises().darElemento(0))) {
				return peli1
						.getGenerosAsociados()
						.darElemento(0)
						.getGenero()
						.compareTo(peli2.getGenerosAsociados().darElemento(0)
								.getGenero()); 

				
			}
			return ((String) peli1.getPaises().darElemento(0)).compareTo((String)peli2.getPaises().darElemento(0));
		}
		return peli1.getAnio()-peli2.getAnio();
	}

	
	

}
