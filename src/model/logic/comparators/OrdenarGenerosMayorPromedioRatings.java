package model.logic.comparators;

import java.util.Comparator;

import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class OrdenarGenerosMayorPromedioRatings implements Comparator<VOGeneroPelicula>
{

	@Override
	public int compare(VOGeneroPelicula arg0, VOGeneroPelicula arg1) {
		//ratingIMBD
		int countArg0 = 0;
		double average0 = 0.0;
		for(VOPelicula movieAct : arg0.getPeliculas())
		{
			countArg0+=movieAct.getCantidadRatings();
			average0+=movieAct.getRatingIMBD();
		}
		int countArg1 = 0;
		double average1 = 0.0;
		for(VOPelicula movieAct : arg1.getPeliculas())
		{
			countArg1+=movieAct.getCantidadRatings();
			average1+=movieAct.getRatingIMBD();
		}
		double averageGenre0, averageGenre1;
		if(countArg0==0)
			averageGenre0=0.0;
		else
			averageGenre0=average0/(double)countArg0;
		if(countArg1==0)
			averageGenre1=0.0;
		else
			averageGenre1=average1/(double)countArg1;
		return Double.compare(averageGenre1, averageGenre0);
	}
	
}
