package model.logic.comparators;

import java.util.Comparator;

import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class OrdenarGenerosCantidadRatings implements Comparator<VOGeneroPelicula>
{

	@Override
	public int compare(VOGeneroPelicula arg0, VOGeneroPelicula arg1) {
		int countArg0 = 0;
		for(VOPelicula movieAct : arg0.getPeliculas())
		{
			countArg0+=movieAct.getCantidadRatings();
		}
		int countArg1 = 0;
		for(VOPelicula movieAct : arg1.getPeliculas())
		{
			countArg1+=movieAct.getCantidadRatings();
		}
		return countArg1-countArg0;
	}
	
}
