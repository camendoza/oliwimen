package model.negocio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

import model.data_structures.ArbolRojoNegro;
import model.data_structures.BST;
import model.data_structures.SeparateChainingHashST;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.MaxHeapCP;
import model.data_structures.NodoValor;
import model.data_structures.Queue;
import model.logic.OrdenarPeliculasReq11;
import model.logic.ShellSort;
import model.logic.comparators.OrdenarGenerosCantidadRatings;
import model.logic.comparators.OrdenarGenerosMayorPromedioRatings;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPrioridadPelicula;
import model.vo.VORating;
import model.vo.VOReporteSegmento;
import model.vo.VOSPeliculas;
import model.vo.VOSimilitudes;
import model.vo.VOTag;
import model.vo.VOTagCategoria;
import model.vo.VOUsuarioPelicula;
import model.vo.VOUsuarioSegmento;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas
{

	private SeparateChainingHashST<Long, VOSimilitudes> listaDeSimilitudes;
	private SeparateChainingHashST<Long, VOPelicula> peliculasPorIdTH;
	private ListaEncadenada<VOPelicula> misPeliculas;
   private SeparateChainingHashST<Long, ListaEncadenada<VORating>> ratingsTH;
	private SeparateChainingHashST<Long, ListaEncadenada<VOTag>> tags;
	private SeparateChainingHashST<String, ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>>> tablaGen;
	private SeparateChainingHashST<String, ListaEncadenada<VOUsuarioSegmento>> usuariosSegmento;
	private SeparateChainingHashST<Long, ListaEncadenada<VOUsuarioPelicula>> usuarios;


	private BST<String, VOTagCategoria> diccionario;

	@Override
	public ISistemaRecomendacionPeliculas crearSR() {
		//peliculas = new SeparateChainingHashST<Integer, ArbolRojoNegro<Date, VOPelicula>>(20);
		misPeliculas = new ListaEncadenada<>();
		tablaGen = new SeparateChainingHashST<>();
		usuariosSegmento = new SeparateChainingHashST<>(4);
		cargarDiccionario("data/tags.csv");
		return this;
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		crearSR();
		System.out.println("******************************");
		VOPelicula movie;
		DateFormat format = new SimpleDateFormat("ddMMMyyyy", Locale.ENGLISH);
		JsonParser jParse = new JsonParser();
		JsonElement jElem;
		try {
			jElem = jParse.parse(new FileReader("./data/links_json.json"));
			JsonArray jVect = jElem.getAsJsonArray();
			JsonPrimitive jTitulo, jFecha, jRating, jGeneros, jVotosTotales, jAnio,jPais;
			String titulo, fecha,  rating;
			
			int votosTotales, year;
			ILista<VOGeneroPelicula> generos;
			Long movieId;
			Date fechaLanzamiento = new Date();
			double ratingIMDB;

			for(int j = 0; j < jVect.size(); j++)
			{
				JsonObject jobject = jVect.get(j).getAsJsonObject();
				JsonPrimitive id = jobject.getAsJsonPrimitive("movieId");
				movieId = new Long(id.getAsInt());
				JsonObject imdbData = jobject.getAsJsonObject("imdbData");
				jTitulo = imdbData.getAsJsonPrimitive("Title");
				titulo = jTitulo.getAsString();
				
				jPais  = imdbData.getAsJsonPrimitive("Country");
				String paisesTemp[] = jPais.getAsString().split(",");
				 ListaEncadenada	paises =new ListaEncadenada<String>();
					for(int i=0; i< paisesTemp.length; i++)
					{
			        paises.agregarElementoFinal(paisesTemp[i]);
					}
				
				jFecha = imdbData.getAsJsonPrimitive("Released");
				fecha = jFecha.getAsString().replace(" ","");
				if(fecha.equals("N/A")){}
				else
					fechaLanzamiento = format.parse(fecha);
				    jAnio = imdbData.getAsJsonPrimitive("Year");
				    {
				    year =0;
				    try{
				     year = jAnio.getAsInt();
				    }
				    catch (Exception e )
				    {
				    	char[] arreglo = jAnio.getAsString().toCharArray();
				    	String aux ="";
				    	for(int i=0;i<4;i++)
				    	{
				    		aux+=arreglo[i];
				    	}
				    	//year = Integer.valueOf(jAnio.getAsString().split("�")[0]).intValue();
				    	year = Integer.parseInt(aux);
				    	
				    }
//}

				jRating  = imdbData.getAsJsonPrimitive("imdbRating");
				rating = jRating.getAsString();
				jVotosTotales = imdbData.getAsJsonPrimitive("imdbVotes");
				String votosTemp[] = jVotosTotales.getAsString().split(",");
				String votosTempString="";
				for(int i = 0; i < votosTemp.length; i++)
				{   if(!votosTemp[i].equals("N/A"))
					votosTempString += votosTemp[i];
				}
				if(votosTempString.equals("")) votosTempString="0";
				votosTotales =0;
				try
				{ votosTotales = Integer.parseInt(votosTempString);
				}
				catch (Exception e)
				{
					System.out.println("Error2"+e.getMessage());
				}

				jGeneros = imdbData.getAsJsonPrimitive("Genre");
				String genresTemp[] = jGeneros.getAsString().split(",");
				generos=new ListaEncadenada<VOGeneroPelicula>();
				for(int i=0; i< genresTemp.length; i++)
				{
					VOGeneroPelicula voTemp = new VOGeneroPelicula();
					voTemp.setGenero(genresTemp[i]);
					generos.agregarElementoFinal(voTemp);
				}

				if(rating.equals("N/A"))
					ratingIMDB = 0.0;
				else
					ratingIMDB = Double.parseDouble(rating);
				movie = new VOPelicula();
				movie.setNombre(titulo);
				movie.setMovieId(movieId);
				movie.setFechaLanzamiento(fechaLanzamiento);
				movie.setGenerosAsociados(generos);
				movie.setVotostotales(votosTotales);
				movie.setRatingIMBD(ratingIMDB);
				movie.setAnio(year);
				movie.setPais(paises);
				if(year==2016)
					movie.setPromedioAnualVotos(votosTotales);
				else
					movie.setPromedioAnualVotos(votosTotales/(2016-year));
				if(peliculasPorIdTH!=null)
				peliculasPorIdTH.put(movie.getMovieId(), movie);
				misPeliculas.agregarElementoFinal(movie);
			}
				    
			}
		
			//peliculas.put(movie.getFechaLanzamiento(), movie);
			
			
			// Se crea una tabla hash en donde cada posicion se encuentra la lista de peliculas
			// de un genero en particular. Esta lista esta representada en un arbol binario ordenado por fecha.
			//******************************* . ***************************
			tablaGen = new SeparateChainingHashST<>();

			for(VOPelicula peliAct: misPeliculas)
			{
				
				for(VOGeneroPelicula genAct: peliAct.getGenerosAsociados())
				{
					if(tablaGen.get(genAct.getGenero().trim())==null)
					{
						ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> nuevoArbol = new ArbolRojoNegro<>();
						ListaEncadenada<VOPelicula> nuevaLista = new ListaEncadenada<>();
						nuevaLista.agregarElementoFinal(peliAct);
					    nuevoArbol.put(peliAct.getFechaLanzamiento(), nuevaLista);
						tablaGen.put(genAct.getGenero().trim(), nuevoArbol);
						
					}
					else
					{  
						if(tablaGen.get(genAct.getGenero().trim()).get(peliAct.getFechaLanzamiento())!=null)
						{
							tablaGen.get(genAct.getGenero().trim()).get(peliAct.getFechaLanzamiento()).agregarElementoFinal(peliAct);
						}
						else
						{
							ListaEncadenada<VOPelicula> nuevaLista =  new ListaEncadenada<VOPelicula>();
							nuevaLista.agregarElementoFinal(peliAct);
							tablaGen.get(genAct.getGenero().trim()).put(peliAct.getFechaLanzamiento(), nuevaLista);
						}

					}
				}
			}
		
		for(String gen: tablaGen.keys())
		{
		System.out.println("El genero: "+gen +" cuenta con "+tablaGen.get(gen).size()+" peliculas");
		
		}
		}
		catch (JsonIOException | JsonSyntaxException | FileNotFoundException | ParseException e) 
		{
			e.printStackTrace();
			System.out.println("Ocurrio un error al cargar el archivo Json");
			return false;
		}
		System.out.println("El tamanio de la lista mis peliculas es: "+misPeliculas.darNumeroElementos());
	    System.out.println("El tamanio de la lista de generos es: "+tablaGen.size());
		return true;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRaitings) {
		BufferedReader br = null;
		try 
		{
			br = new BufferedReader(new FileReader(rutaRaitings));
			String linea = br.readLine();
			linea = br.readLine();
			int ind = 1;
			try
			{
				while(linea!=null&&ind<3355)
				{
					linea=br.readLine();
					String[]casillas=linea.split(",");
					VORating nuevoRating = new VORating();
					nuevoRating.setIdUsuario(Integer.parseInt(casillas[0]));
					nuevoRating.setIdPelicula(Integer.parseInt(casillas[1]));
					nuevoRating.setRating(Double.parseDouble(casillas[2]));
					nuevoRating.setTimestap(Long.parseLong(casillas[3]));
					nuevoRating.setError(Double.parseDouble(casillas[4]));
					if(ratingsTH.contains(nuevoRating.getIdUsuario()))
					{
						ratingsTH.get(nuevoRating.getIdUsuario()).agregarElementoFinal(nuevoRating);
					}
					else
					{
						ListaEncadenada<VORating> nuevaLista = new ListaEncadenada<VORating>();
						nuevaLista.agregarElementoFinal(nuevoRating);
						ratingsTH.put(nuevoRating.getIdUsuario(),nuevaLista);
					}
					ind++;
				
				}
				return true;
			}
			catch(Exception e)
			{
				System.out.println("Error: " + e.getMessage());
				return false;
			}
		}
		catch (Exception e)
		{
			System.out.println("Error 2"+ e.getMessage());
			return false;
		}
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		BufferedReader br =null;
		try
		{
			br = new BufferedReader(new FileReader(rutaTags));
			String linea = br.readLine();
			linea = br.readLine();
			int ind = 1;
			try
			{

				while(linea!=null&&ind<1296)
				{
					linea=br.readLine();
					VOTag nuevo = new VOTag();
					if(!linea.equals(""))
					{
						if(linea.indexOf("\"") == -1)
						{
							String[] casillas = linea.split(",");
							nuevo.setIdUsuario(Long.parseLong(casillas[0]));
							nuevo.setIdMovie(Long.parseLong(casillas[1]));
							nuevo.setContenido(casillas[2]);
							nuevo.setTimestamp(Long.parseLong(casillas[3]));
							if(tags.contains(nuevo.getIdUsuario())){
								tags.get(nuevo.getIdUsuario()).agregarElementoFinal(nuevo);
							}else{
								ListaEncadenada<VOTag> aux =  new ListaEncadenada<VOTag>();
								aux.agregarElementoFinal(nuevo);
								tags.put(nuevo.getIdUsuario(), aux);
							}
							ind++;
						}
						else
						{
							String[] casillas = linea.split("\"");
							if(casillas.length==3)
							{
								String primerosDos = casillas[0];
								String tagss = casillas [1];
								String timeStap = casillas[2].split(",")[1];

								nuevo.setIdUsuario(Long.parseLong(primerosDos.split(",")[0]));
								nuevo.setIdMovie(Long.parseLong(primerosDos.split(",")[1]));
								nuevo.setContenido(tagss);
								nuevo.setTimestamp(Long.parseLong(timeStap));
								if(tags.contains(nuevo.getIdUsuario())){
									tags.get(nuevo.getIdUsuario()).agregarElementoFinal(nuevo);
								}else{
									ListaEncadenada<VOTag> aux =  new ListaEncadenada<VOTag>();
									aux.agregarElementoFinal(nuevo);
									tags.put(nuevo.getIdUsuario(), aux);
								}
								ind++;
							}else{
								String[] casillas2 = linea.split(",");
								nuevo.setIdUsuario(Long.parseLong(casillas2[0]));
								nuevo.setIdMovie(Long.parseLong(casillas2[1]));
								nuevo.setContenido(casillas2[2]);
								nuevo.setTimestamp(Long.parseLong(casillas2[3]));
								if(tags.contains(nuevo.getIdUsuario())){
									tags.get(nuevo.getIdUsuario()).agregarElementoFinal(nuevo);
								}else{
									ListaEncadenada<VOTag> aux =  new ListaEncadenada<VOTag>();
									aux.agregarElementoFinal(nuevo);
									tags.put(nuevo.getIdUsuario(), aux);
								}
								ind++;
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("Error: "+e.getMessage());
				return false;
			}
		}
		catch(Exception e)
		{
			System.out.println("Error 2: "+e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public int sizeMoviesSR() {
		return misPeliculas.darNumeroElementos();
	}

	public int sizeUsersSR() {

		return usuarios.size();
	}

	@Override
	public int sizeTagsSR() {

		return tags.size();
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) {


	}

	@Override
	public void generarRespuestasRecomendaciones() {

	}

	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
	
		ListaEncadenada<VOPelicula> resp = new ListaEncadenada<>();
		if(tablaGen == null)
		{
			System.out.println("La tabla de generos es nula");
		}
		if(tablaGen.get(genero.getGenero())!=null);
		ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> arbolDelGenero = tablaGen.get(genero.getGenero().trim());
		if(arbolDelGenero!=null)
		{
		ListaEncadenada<ListaEncadenada<VOPelicula>> listaDeListas = arbolDelGenero.inorden(arbolDelGenero.getRoot());
		
		for(ListaEncadenada<VOPelicula> listAct : listaDeListas)
			
	    {	if(listAct.darElemento(0)!=null)
	    {
	    	
			int comparador1 = listAct.darElemento(0).getFechaLanzamiento().compareTo(fechaInicial);
			int comparador2 = listAct.darElemento(0).getFechaLanzamiento().compareTo(fechaFinal);
			if(comparador2>0)
			{
				System.out.println("el numero de peliculas entre el rango de fechas es: "+resp.darNumeroElementos());
				return resp;
			}
			else if(comparador1>=0&&comparador2<=0)
			{
				for(VOPelicula act: listAct)
				resp.agregarElementoFinal(act);
				
			}
	    }	
	    }
		}
		System.out.println("el numero de peliculas entre el rango de fechas es: "+resp.darNumeroElementos());
		return resp;
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating) {
		///Este error es provisional, falta el metodo para calcularlo
		double ratingCalculado=0.0;
		double errorCalculado =Math.abs(rating-ratingCalculado);

		VORating nuevoRating = new VORating();
		nuevoRating.setError(errorCalculado);
		nuevoRating.setIdPelicula(idPelicula);
		nuevoRating.setIdUsuario(idUsuario);
		nuevoRating.setRating(rating);
		
		if(ratingsTH.get(Long.parseLong(String.valueOf(idUsuario)))!=null)
			ratingsTH.get(Long.parseLong(String.valueOf(idUsuario))).agregarElementoFinal(nuevoRating);
		else
		{
			ListaEncadenada<VORating> nuevaListaPorUsuario = new ListaEncadenada<>();
			nuevaListaPorUsuario .agregarElementoFinal(nuevoRating);
		ratingsTH.put(nuevoRating.getIdUsuario(), nuevaListaPorUsuario);
		}


	}

	public void crearListaUsuarios()
	{
		usuarios = new SeparateChainingHashST<Long, ListaEncadenada<VOUsuarioPelicula>>(671);
		Queue<Long> cola = (Queue) ratingsTH.keys();
		while(!cola.isEmpty())
		{
			for(VORating ratAct: ratingsTH.get(cola.dequeue()))
			{
				VOUsuarioPelicula nuevoUsuario = new VOUsuarioPelicula();
				nuevoUsuario.setIdUsuario(Integer.valueOf((String.valueOf(ratAct.getIdUsuario()))));
				nuevoUsuario.setIdPelicula(Integer.valueOf((String.valueOf(ratAct.getIdPelicula()))));
				nuevoUsuario.setRatingUsuario(ratAct.getRating());
				nuevoUsuario.setErrorRating(ratAct.getError());
				nuevoUsuario.setTags(tags.get(Long.valueOf(String.valueOf(nuevoUsuario.getIdUsuario()))));
				if(!usuarios.contains(ratAct.getIdUsuario()))
				{
					ListaEncadenada<VOUsuarioPelicula> nuevaLista = new ListaEncadenada<VOUsuarioPelicula>();
					nuevaLista.agregarElementoFinal(nuevoUsuario);
					usuarios.put(Long.valueOf(String.valueOf(nuevoUsuario.getIdUsuario())), nuevaLista);
				}
				else
					usuarios.get(ratAct.getIdUsuario()).agregarElementoFinal(nuevoUsuario);
			}
		}

	}

	@Override
	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) {

		if(usuarios==null)
		{
			return null;
		}
		return usuarios.get(Long.valueOf(String.valueOf(idUsuario)));
		}


	@Override
	public void clasificarUsuariosPorSegmento() {
		for(Long key : tags.keys())
		{
			ListaEncadenada<VOTag> listTagsUserActual = tags.get(key);
			long idUser = listTagsUserActual.darElemento(0).getIdUsuario();
			// Counts
			int conf=0, inconf=0, neut=0, noClas=0;
			for(VOTag tagUserActual : listTagsUserActual)
			{
				String tagContent = tagUserActual.getContenido();
				VOTagCategoria tagCategory = diccionario.get(tagContent);
				switch (tagCategory.getCategoria()) {
				case "conforme":
					conf++;
					break;
				case "inconforme":
					inconf++;
					break;
				case "neutral":
					neut++;
					break;
				case "no-clasificado":
					noClas++;
					break;
				default:
					break;
				}
			}
			VOUsuarioSegmento userSegmentActual = new VOUsuarioSegmento();
			ListaEncadenada<VORating> ratingsToAdd = ratingsTH.get(idUser);

			userSegmentActual.setIdUsuario(idUser);			
			userSegmentActual.setRatings(ratingsToAdd);
			if(conf>=inconf&&conf>=neut&&conf>=noClas)
			{
				if(usuariosSegmento.contains("conforme"))
				{
					usuariosSegmento.get("conforme").agregarElementoFinal(userSegmentActual);
				}else{
					ListaEncadenada<VOUsuarioSegmento> toAdd = new ListaEncadenada<>();
					toAdd.agregarElementoFinal(userSegmentActual);
					usuariosSegmento.put("conforme", toAdd);
				}
			}else if(inconf>=neut&&inconf>=noClas&&inconf>=conf)
			{
				if(usuariosSegmento.contains("inconforme"))
				{
					usuariosSegmento.get("inconforme").agregarElementoFinal(userSegmentActual);
				}else{
					ListaEncadenada<VOUsuarioSegmento> toAdd = new ListaEncadenada<>();
					toAdd.agregarElementoFinal(userSegmentActual);
					usuariosSegmento.put("inconforme", toAdd);
				}
			}else if(neut>=noClas&&neut>=conf&&neut>=inconf)
			{
				if(usuariosSegmento.contains("neutral"))
				{
					usuariosSegmento.get("neutral").agregarElementoFinal(userSegmentActual);
				}else{
					ListaEncadenada<VOUsuarioSegmento> toAdd = new ListaEncadenada<>();
					toAdd.agregarElementoFinal(userSegmentActual);
					usuariosSegmento.put("neutral", toAdd);
				}
			}else{
				if(usuariosSegmento.contains("no-clasificado"))
				{
					usuariosSegmento.get("no-clasificado").agregarElementoFinal(userSegmentActual);
				}else{
					ListaEncadenada<VOUsuarioSegmento> toAdd = new ListaEncadenada<>();
					toAdd.agregarElementoFinal(userSegmentActual);
					usuariosSegmento.put("no-clasificado", toAdd);
				}
			}
		}
	}

	@Override
	public void ordenarPeliculasPorAnho() {
		// TODO Auto-generated method stub
		ArbolRojoNegro<Integer, SeparateChainingHashST<String, ArbolRojoNegro<Double, ListaEncadenada<VOPelicula>>>> 
		resp = new ArbolRojoNegro<>();
		
		for(VOPelicula peliAct: misPeliculas)
		{
			// Si el anio de la pelicula actual aun no se encuentra en la tabla, se deben crear c/u de las esctructuras.
			if(resp.get(peliAct.getAnio())==null)
			{
				SeparateChainingHashST<String,ArbolRojoNegro<Double, ListaEncadenada<VOPelicula>>> tablaN1 = new SeparateChainingHashST<>();
				ArbolRojoNegro<Double, ListaEncadenada<VOPelicula>> arbolN1 = new ArbolRojoNegro<>();
				ListaEncadenada<VOPelicula> listaN1 = new ListaEncadenada<>();
				
				listaN1.agregarElementoFinal(peliAct);
				arbolN1.put(peliAct.getRatingIMBD(),listaN1);
				for(VOGeneroPelicula gAct : peliAct.getGenerosAsociados())
				{
					tablaN1.put(gAct.getGenero(), arbolN1);
				}
				resp.put(peliAct.getAnio(), tablaN1);
				
			}
			else
			{
				//Se verifica si la tabla posee todos los generos de la pelicula actual
				for(VOGeneroPelicula gAct: peliAct.getGenerosAsociados())
				{
				if(resp.get(peliAct.getAnio()).get(gAct.getGenero())==null)
				{
					ArbolRojoNegro<Double, ListaEncadenada<VOPelicula>> arbolN1 = new ArbolRojoNegro<>();
					ListaEncadenada<VOPelicula> listaN1=  new ListaEncadenada<>();
					listaN1.agregarElementoFinal(peliAct);
					arbolN1.put(peliAct.getRatingIMBD(), listaN1);
					resp.get(peliAct.getAnio()).put(gAct.getGenero(), arbolN1);
				}
				
				else
				{
					//Si la arbol aun no posee el rating de la pelicula actual
					if(resp.get(peliAct.getAnio()).get(gAct.getGenero()).get(peliAct.getRatingIMBD())==null)
					{
						ListaEncadenada<VOPelicula> listaN1 = new ListaEncadenada<>();
						listaN1.agregarElementoFinal(peliAct);
						resp.get(peliAct.getAnio()).get(gAct.getGenero()).put(peliAct.getRatingIMBD(), listaN1);
						
					}
				}
				}
			}
		}
		System.out.println("El arbol resultante del req 7 tiene un tamanio de: "+resp.size());
	}

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		ListaEncadenada<VOUsuarioSegmento> usuarios = usuariosSegmento.get(segmento);

		ILista<VOPelicula> moviesForSegment = new ListaEncadenada<VOPelicula>();
		SeparateChainingHashST<Long, VOPelicula> moviesForSegmentTH = new SeparateChainingHashST<>(100);

		double sumaErrores = 0.0;
		double cantidadRatings = 0.0;
		for(VOUsuarioSegmento usuarioSegmentoActual : usuarios)
		{
			ListaEncadenada<VORating> ratings = usuarioSegmentoActual.getRatings();
			cantidadRatings+=ratings.darNumeroElementos();
			for(VORating ratingActual : ratings)
			{
				sumaErrores+=ratingActual.getError();
				long idPelicula = ratingActual.getIdPelicula();
				VOPelicula peliculaToAdd = peliculasPorIdTH.get(idPelicula);
				if(peliculaToAdd!=null){
					if(!moviesForSegmentTH.contains(peliculaToAdd.getMovieId()))
						moviesForSegmentTH.put(peliculaToAdd.getMovieId(), peliculaToAdd);
				}
			}
		}
		for(Long idActual : moviesForSegmentTH.keys())
			moviesForSegment.agregarElementoFinal(moviesForSegmentTH.get(idActual));

		ILista<VOGeneroPelicula> toSort = agruparPorGenero(moviesForSegment);
		ShellSort.Sort(toSort, new OrdenarGenerosCantidadRatings());

		ILista<VOGeneroPelicula> moreRatingsToAdd = new ListaEncadenada<VOGeneroPelicula>();
		for(int i = 0; i < 5; i++)
		{
			moreRatingsToAdd.agregarElementoFinal(toSort.darElemento(i));
		}

		ShellSort.Sort(toSort, new OrdenarGenerosMayorPromedioRatings());
		ILista<VOGeneroPelicula> bestRatingAverageToAdd = new ListaEncadenada<VOGeneroPelicula>();
		for(int i = 0; i < 5; i++)
		{
			bestRatingAverageToAdd.agregarElementoFinal(toSort.darElemento(i));
		}

		// to return
		VOReporteSegmento toReturn  = new VOReporteSegmento();
		toReturn.setErrorPromedio(sumaErrores/cantidadRatings);
		toReturn.setGenerosMasRatings(moreRatingsToAdd);
		toReturn.setGenerosMejorPromedio(bestRatingAverageToAdd);
		return toReturn;
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial,
			Date fechaFinal) {
		ListaEncadenada<VOPelicula> resp = new ListaEncadenada<>();
		if(tablaGen == null)
		{
			System.out.println("La tabla de generos es nula");
		}
		if(tablaGen.get(genero.getGenero())!=null);
		ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> arbolDelGenero = tablaGen.get(genero.getGenero().trim());
		if(arbolDelGenero!=null)
		{
		ListaEncadenada<ListaEncadenada<VOPelicula>> listaDeListas = arbolDelGenero.inorden(arbolDelGenero.getRoot());
		
		for(ListaEncadenada<VOPelicula> listAct : listaDeListas)
	  {	
			if(listAct.darElemento(0)!=null)
	  		{
		   int comparador1 = listAct.darElemento(0).getFechaLanzamiento().compareTo(fechaInicial);
			int comparador2 = listAct.darElemento(0).getFechaLanzamiento().compareTo(fechaFinal);
			if(comparador2>0)
			{
				System.out.println("1° El numero de peliculas entre el rango de fechas es: "+resp.darNumeroElementos());
				return resp;
			}
			else if(comparador1>=0&&comparador2<=0)
			{
				for(VOPelicula act: listAct)
				{ /////////////////MODIFICACION
					if(!resp.contieneElemento(act))
				    resp.agregarElementoFinal(act);
				}
			}
	    }	
	    }
		}
		//System.out.println("2° El numero de peliculas entre el rango de fechas es: "+resp.darNumeroElementos());
		return resp;
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {
		ILista<VOPelicula> toReturn = new ListaEncadenada<>();
		MaxHeapCP<VOPrioridadPelicula> auxHeap = new MaxHeapCP<>();
		for(VOPelicula movieAct : misPeliculas)
		{
			VOPrioridadPelicula auxPrioridad = new VOPrioridadPelicula(movieAct);
			auxHeap.agregar(auxPrioridad);
		}
		for(int i = 0; i < n; i++)
		{
			VOPrioridadPelicula act = auxHeap.max();
			VOPelicula toAdd = act.getPelicula();
			toReturn.agregarElementoFinal(toAdd);
		}
		return toReturn;
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {

		ListaEncadenada<VOPelicula> resp = new ListaEncadenada<>();
		if(anho == 0 && pais==null && genero==null)return null;
		
		else if(anho == 0 && pais!=null && genero==null)
		{
           for(VOPelicula peliAct:misPeliculas)
			{
        	   if(peliAct.getPaises().contieneElemento(pais))
			      resp.agregarElementoFinal(peliAct);
			  }
      
		}
		else if(anho==0&&pais!=null&&genero!=null)
		{
            ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> arbolDeGenero = tablaGen.get(genero.getGenero().trim());
            ListaEncadenada<ListaEncadenada<VOPelicula>> listaPelDelGenero = (ListaEncadenada<ListaEncadenada<VOPelicula>>)arbolDeGenero.inorden(arbolDeGenero.getRoot());
            for(ListaEncadenada<VOPelicula> l: listaPelDelGenero)
            {
            	if(l!=null)
            	for(VOPelicula peli: l)
            	{
            	if(peli.getPaises().contieneElemento(pais))
            	{
            		resp.agregarElementoFinal(peli);
            	}
            	}
            }
		}

		else if(anho == 0 && pais==null && genero!=null)
		{
			ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> arbolDeGenero = tablaGen.get(genero.getGenero().trim());
			ListaEncadenada<ListaEncadenada<VOPelicula>> p = arbolDeGenero.inorden(arbolDeGenero.getRoot());
			for(ListaEncadenada<VOPelicula> primeraLista: p)
			{
				if(primeraLista!=null)
				for(VOPelicula peliAct: primeraLista)
				{
					resp.agregarElementoFinal(peliAct);
				}
			}
            
			
		}
		else if(anho!= 0 && pais==null && genero==null)
		{
			for(VOPelicula peliAct: misPeliculas)
			{
				if(peliAct.getAnio()==anho)
				{
					resp.agregarElementoFinal(peliAct);
				}
			}
		}

		

		else if(anho!=0&&pais!=null&&genero==null)
		{
		  for(VOPelicula peliAct: misPeliculas)
		  {
			  if(peliAct.getAnio()==anho&&peliAct.getPaises().contieneElemento(pais))
			  {
				  resp.agregarElementoFinal(peliAct);
			  }
		  }
		}

		else if(anho!=0&&pais==null&&genero!=null)
		{
			ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> peliculasDelGenero = tablaGen.get(genero.getGenero().trim());
			ListaEncadenada<ListaEncadenada<VOPelicula>> listPelis = peliculasDelGenero.inorden(peliculasDelGenero.getRoot());
			for(ListaEncadenada<VOPelicula> lista: listPelis)
			{
				if(lista!=null)
				for(VOPelicula peliAct: lista)
				{
				if(peliAct.getAnio()==anho)
				resp.agregarElementoFinal(peliAct);
				}
			}
			
		}

		else if(anho!=0&&pais!=null&&genero!=null)
		{

			ArbolRojoNegro<Date, ListaEncadenada<VOPelicula>> arbolDeGenero = tablaGen.get(genero.getGenero().trim());
			if(arbolDeGenero!=null)
			{
			ListaEncadenada<ListaEncadenada<VOPelicula>> p = arbolDeGenero.inorden(arbolDeGenero.getRoot());
			for(ListaEncadenada<VOPelicula> primeraLista: p)
			{
				if(primeraLista!=null)
				for(VOPelicula peliAct: primeraLista)
				{
					resp.agregarElementoFinal(peliAct);
				}
			}
			}
			
		}
		
		/// ****** SE DEBE ORDENAR RESP ANTES DE RETORNAR LA LISTA******* ///
		ShellSort.Sort(resp, new OrdenarPeliculasReq11());
		return resp;
	}
                                                    

	// --------------------------------------------------------------------------------------------------------------- //
	// --------------------------------------Métodos-auxiliares------------------------------------------------------- //
	// --------------------------------------------------------------------------------------------------------------- //
	public ILista<VOGeneroPelicula> agruparPorGenero(ILista<VOPelicula> misPeliculas)
	{
		ILista<VOGeneroPelicula> listaGeneros = new ListaEncadenada<VOGeneroPelicula>();

		ListaEncadenada<String> generosActual = new ListaEncadenada<>();

		for (VOPelicula peliculaActual : misPeliculas) {
			for(VOGeneroPelicula generoActualAux : peliculaActual.getGenerosAsociados())
			{
				String nombreGenero = generoActualAux.getGenero();
				generosActual.agregarElementoFinal(nombreGenero);
			}

			for (String generoActualPeliculas : generosActual) {


				if (listaGeneros.darNumeroElementos()==0) {
					VOGeneroPelicula nuevoGenero = new VOGeneroPelicula();
					ListaEncadenada<VOPelicula> nuevaListaPeliculas = new ListaEncadenada<>();
					nuevaListaPeliculas.agregarElementoFinal(peliculaActual);
					nuevoGenero.setGenero(generoActualPeliculas);
					nuevoGenero.setPeliculas(nuevaListaPeliculas);
					listaGeneros.agregarElementoFinal(nuevoGenero);

				}
				else
				{
					int ind =0;
					boolean encontro = false;
					for (VOGeneroPelicula generoDeListaGeneros : listaGeneros) 
					{
						if (generoActualPeliculas.equals(generoDeListaGeneros.getGenero())) {
							generoDeListaGeneros.getPeliculas().agregarElementoFinal(peliculaActual);
							encontro = true;
						}
						ind++;
					} 
					if(encontro==false&&ind==listaGeneros.darNumeroElementos())
					{   VOGeneroPelicula nuevoGenero = new VOGeneroPelicula();
					ListaEncadenada<VOPelicula> nuevaListaPeliculas = new ListaEncadenada<>();
					nuevaListaPeliculas.agregarElementoFinal(peliculaActual);
					nuevoGenero.setGenero(generoActualPeliculas);
					nuevoGenero.setPeliculas(nuevaListaPeliculas);
					listaGeneros.agregarElementoFinal(nuevoGenero);
					ind++;
					}

				}
			}
		}
		return listaGeneros;
	}

	public boolean cargarDiccionario(String rutaTags) {
		BufferedReader br = null;
		try 
		{
			br = new BufferedReader(new FileReader(rutaTags));
			String linea = br.readLine();
			diccionario = new BST<String, VOTagCategoria>();
			linea = br.readLine();
			int ind = 1;
			try
			{
				while(linea!=null&&ind<560)
				{
					linea=br.readLine();
					String[]casillas=linea.split(",");
					VOTagCategoria newTagCategoria = new VOTagCategoria();
					newTagCategoria.setTag(casillas[0]);
					newTagCategoria.setCategoria(casillas[1]);
					diccionario.put(casillas[0], newTagCategoria);
					ind++;
					//System.out.println(ind +" "+nuevoRating.getRating() );
				}
			}
			catch(Exception e)
			{
				System.out.println("Error: " + e.getMessage());
				return false;
			}
		}
		catch (Exception e)
		{
			System.out.println("Error 2"+ e.getMessage());
			return false;
		}
		return true;
	}
	
	
	
	
	/// ****************************************
	
//	public void  formarSimilitud()
//	{
//		Iterator<VOPelicula> iterador = misPeliculas.iterator();
//		while(iterador.hasNext())
//		{crearSimilitud(iterador.next());
//		}
//	}
//	private void  crearSimilitud(VOPelicula id)
//	{
//		Long movieId = id.getMovieId();
//		Iterator<VOPelicula> iterador = misPeliculas.iterator();
//		ListaEncadenada<VOSPeliculas> listaS = new ListaEncadenada<VOSPeliculas>();
//		while(iterador.hasNext())
//		{
//			VOPelicula pelActual = iterador.next();
//			Long movieId2 = pelActual.getMovieId();
//			if(pelActual.getMovieId()!=movieId)
//			{
//				listaS.agregarAlPrincipio(darSimilitud(movieId, movieId2));
//			}
//		}
//		VOSimilitudes simPAct = new VOSimilitudes();
//		simPAct.setMovieId(movieId);
//		simPAct.setListaSim(listaS);
//		listaDeSimilitudes.put(movieId, simPAct);
//
//	}
//
//	private VOSPeliculas darSimilitud( Long movieId1 ,Long movieId2)
//	{
//		ListaEncadenada<VOPelCompRating> lista = new ListaEncadenada<VOPelCompRating>();
//		Iterator<Long> iteradorLlaves = (Iterator<Long>) usuarios2.keys();
//		while(iteradorLlaves.hasNext())
//		{
//			
//			Long  llaveActual = iteradorLlaves.next();
//			ListaEncadenada nod = usuarios2.get(llaveActual);
//			VOUsuario ud = (VOUsuario) nod.get();
//			ListaEncadenada<VOUsuarioPelicula> listaPeliculasRan = ud.getNose();
//			Iterator<VOUsuarioPelicula> iterLista = listaPeliculasRan.iterador();
//
//			while(iterListaPelR.hasNext())
//			{
//				Double ratingP1 = null;
//				Double ratingP2 = null;
//				VOUsuarioPelicula userActual = iterListaPelR.next();
//				if(userActual.getIdPelicula()== movieId1)
//				{
//					ratingP1 = userActual.getRatingUsuario();
//				}
//				if(userActual.getIdPelicula()== movieId2)
//				{
//					ratingP2 = userActual.getRatingUsuario();
//				}
//				VOPelCompRating compActual = new VOPelCompRating();
//				compActual.setmovieId1(movieId1); compActual.setmovieId2(movieId2);
//				compActual.setRatingP1(ratingP1); compActual.setRatingP2(ratingP2);
//				lista.agregarAlPrincipio(compActual);
//			}
//		}

	
	}
	
	




























