/*
 * La documentaci�n del API de ninguna forma reemplaza la lectura de los requerimientos del proyecto.
 * El documento del proyecto tiene definidos los requerimientos exactos.
 * 
 * @Author: Tomas F. Venegas
 * 
 */

package model.negocio;

import java.util.Date;

import model.data_structures.ILista;
import model.vo.*;

public interface ISistemaRecomendacionPeliculas {
	
	public enum segmentosClientes{
		 Inconformes, 
		 Conformes,
		 Neutrales, 
		 No_Clasificados,
		 
	};
	
	/**
	 * Metodo que crea un Sistema de recomendaci�n vacio
	 * @return ISistemaRecomendacionPeliculas
	 */
	ISistemaRecomendacionPeliculas crearSR();
	
	/**
	 * carga las pel�clas al sistemas
	 * 
	 * @param rutaPeliculas
	 * @return
	 */
	boolean cargarPeliculasSR(String rutaPeliculas);
	
	/**
	 * carga los ratings al sistema
	 * 
	 * @param rutaRaitings
	 * @return
	 */
	boolean cargarRatingsSR(String rutaRaitings);
	
	/**
	 * carga los tags al sistema
	 * 
	 * @param rutaTags
	 * @return
	 */
	boolean cargarTagsSR(String rutaTags);
	
	/**
	 * 
	 * @return n�mero de pel�clas en el sistema
	 */
	int sizeMoviesSR() ;
	
	/**
	 * 
	 * @return n�mero de usuarios registrados
	 */
	int sizeUsersSR() ;
	
	/**
	 * 
	 * @return n�mero de tags registrados en el sistema
	 */
	int sizeTagsSR() ;
	
	/**
	 * R1
	 * 
	 * registra la solicitud de un cliente de recibir recomendaciones.
	 * Si el usuario est� registrado se pasa su id. Sino se pasa la ruta del archivo JSON
	 * Uno de los dos par�metros ser� null.
	 * @param idUsuario
	 * @param ruta
	 */
	void registrarSolicitudRecomendacion(Integer idUsuario, String ruta);

	/**
	 * R2
	 * 
	 * Genera el archivo con la respuesta a las 10 solicitudes de recomendaciones de major prioridad en
	 * formato JSON
	 * Recomendaciones_<Dia-Mes-A�o_Hora_min_seg>.json
	 * 
	 */
	void generarRespuestasRecomendaciones();
	
	/**
	 * R3
	 * 
	 * 
	 *  
	 * @param genero
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return un listado ordenado por fecha de todas las pel�culas
	 *  del genero genero dentro del rango de fechas dado
	 */
	ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal);
	

	
	/**
	 * R4
	 * 
	 * A�adir un nuevo rating a una pel�cula y guardar el error de predicci�n
	 * @param idUsuario
	 * @param idPelicula
	 * @param rating
	 */
	void agregarRatingConError(int idUsuario, int idPelicula, Double rating);
	
	/**
	 * genera un informe con toda la informaci�n que se tiene sobre un 
	 * usuario y su interacci�n con las pel�culas.
	 * 
	 * @param idUsuario
	 * @return una lista en la que en cada posici�n se tiene el nombre de la pel�cula, el rating otorgado por el usuario, el rating 
	 * calculado por el sistema, el error para el rating y los tags que el usuario le otorg�.
	 */
	ILista<VOUsuarioPelicula> informacionInteraccionUsuario (int idUsuario);
	
	/**
	 * R6
	 * Clasifica los usuarios por segmentos y genera
	 *  listas de identificadores de usuarios de cada segmento.
	 * 
	 */
	void clasificarUsuariosPorSegmento();
	
	/**R7
	 * 
	 * Genera el arbol descrito en R7.
	 * 
	 */
	void ordenarPeliculasPorAnho();
	
	/**
	 * R8
	 * Generar un reporte por segmento, debe inclu�r: el error
	 *promedio  (suma de errores sobre sus ratings dividido la cantidad de ratings con 
	 *error asociado),  sus 5 g�neros con m�s cantidad de ratings y sus 5 g�neros con mejor rating promedio.

	 * @param segmento
	 * @return reporte sobre el segmento
	 */
	VOReporteSegmento generarReporteSegmento(String segmento);
	
	/**
	 * R9
	 * 
	 * 
	 * 
	 * @param genero
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return lista de pel�culas de un genero dado cuya fecha de lanzamiento se encuentra
	 * entre la fechaIncial y la fechaFinal.
	 */
	ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal);
	
	/**
	 * R10
	 * 
	 * Con la utilizaci�n de un heap generar la lista de n pel�culas con mayor prioridad.
	 * Para cada pel�cula  resultante,  hay  que  mostrar  su  t�tulo,  a�o,  votos  totales,  promedio
	 * anual de votos y prioridad de clasificaci�n.
	 * @param n n�mero de pel�culas a retornar
	 * @return lista de n pel�culas con mayor prioridad
	 */
	ILista<VOPelicula> peliculasMayorPrioridad(int n);
	
	/**
	 * R11
	 * Los par�metros de entrada pueden ser null o no dependiendo del cliente.
	 * 
	 * @param anho
	 * @param pais
	 * @param genero
	 * @return lista de pel�culas seg�n los filtros y ordenadas : Inicialmente por a�o, luego por 
	 * pa�s, por g�nero y finalmente por raiting IMBD. (una pel�cula puede tener asociado m�ltiples 
	 * pa�ses y m�ltiples g�neros).
	 */
	ILista<VOPelicula> consultarPeliculasFiltros (Integer anho, String pais, VOGeneroPelicula genero);
	
	
	
	
	
}
