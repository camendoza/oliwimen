package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements Iterable<T>
{

	private ILista<T> lista;
	private NodoSencillo<T> primero;
	private NodoSencillo<T> ultimo;
	private int size;
	
	public Queue()
	{
		lista = new ListaEncadenada<T>();
		size=0;
	}
	public void enqueue(T T)
	{
		NodoSencillo<T> nuevo = new NodoSencillo<T>(T);
		NodoSencillo<T> viejoUltimo = ultimo;
		ultimo = nuevo;
		if(isEmpty())
		{
			primero = ultimo;
		}
			
		else
	    {
			viejoUltimo.cambiarSiguiente(ultimo);
		}
		lista.agregarElementoFinal(ultimo.darItem());
		
		size++;
	}
	public T dequeue()
	{
		T T = primero.darItem();  
	    primero = primero.darSiguiente();
	    if(isEmpty())
	    {
	    	ultimo = null;
	    }
	    size--;     
	    return T; 
	}
	public boolean isEmpty()
	{
		return size==0;
		
	}
	public int size()
	{
		return size;
	}
	@Override
	public Iterator<T> iterator()  {
        return new ListIterator<T>(primero);  
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator<T> implements Iterator<T> {
        private NodoSencillo<T> current;

        public ListIterator(NodoSencillo<T> first) {
            current = first;
        }

        public boolean hasNext()  { return current != null;                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T item = current.darItem();
            current = current.darSiguiente(); 
            return item;
        }
    }
}
