package model.data_structures;

public class NodoValor <K,V >{
	
	private K llave;
	private V valor;
	private NodoValor<K,V> siguiente;
	
	public NodoValor(K pLlave, V pValor)
	{
		siguiente = null;
		llave = pLlave;
		valor= pValor;
	}
	
	public void setLlave(K pLlave)
	{
		llave = pLlave;
	}
	
	public void setValor(V pValor)
	{
		valor = pValor;
	}
	
	public K darLlave()
	{
		return llave;
	}
	
	public V darValor()
	{
		return valor;
	}
	
	public void setSiguiente(NodoValor<K, V> sig)
	{
		siguiente = sig;
	}
	public NodoValor<K, V> darSiguiente()
	{
		return siguiente;
		
	}

	

}
