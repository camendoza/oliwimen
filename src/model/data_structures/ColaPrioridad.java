package model.data_structures;

public class ColaPrioridad <T extends Comparable<T>> {
	
	private Object[] cola;
	private int tamanioMax;
	
	public void crearCP(int max)
	{    
		 cola =   new Object[max] ;
		 tamanioMax=max;
	}
	public int darNumElementos() 
	{
		int cont =0;
		for(int i=0; i<cola.length; i++)
		{
		   	if(cola[i]!=null)
		   	{
		   		cont++;
		   	}
		   	else
		   	{
		   		break;
		   	}
		}
		return cont;
		
	}
	
	public void agregar(T elemento) throws Exception 
	{
		boolean agrego =false;
		for(int i=0; i<cola.length;i++)
		{
			if(cola[i]==null)
			{
				cola[i]=elemento;
				agrego = true;
				break;
			}
		}
		if(agrego == false)
		{
			throw new Exception("No se agreg� el elemento, el arreglo est� lleno");
		}
	}
	public T max()
	{
		T max =null;
		
		if(cola.length!=0)
		{
		   max = (T) cola[0];
		
		  for(int i=0; i<darNumElementos();i++)
		 {
			if(i!=0)
			if(((Comparable<T>) cola[i]).compareTo(max)>0)
			{	max = (T) cola[i];
			    cola[i]=null;
			}   
		 }
		}
		return max;
	}
	
	public boolean esVacia() 
	{
		return darNumElementos()==0;
	}
	
	public int tamanoMax() 
	{
		return tamanioMax;
	}
	
	

}
