package model.data_structures;

 	
	
	
	public class NodoRojoNegro <Key extends Comparable<Key>, Value>
	{
	 private static final boolean RED = true;
	 private static final boolean BLACK = false;
	  public Key key; 
	  public Value val; 
	  public NodoRojoNegro<Key, Value> left, right; 
	  int N; 
	  boolean color; 
	 
	public NodoRojoNegro(Key key, Value val, int N, boolean color)
	 {
	 this.key = key;
	 this.val = val;
	 this.N = N;
	 this.color = color;
	 }
	 
	 public boolean isRed(NodoRojoNegro<Key, Value> x)
	{
		if (x == null) return false;
		return x.color == RED;
    }
	 public NodoRojoNegro<Key, Value> getNodeLeft()
	 {
		 return left;
	 }
	 
	 public NodoRojoNegro<Key, Value> getNodeRight()
	 {
		 return right;
	 }
}
	
	

 
