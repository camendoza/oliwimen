package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private int size;
	private NodoSencillo<T> cabeza;
	private NodoSencillo<T> actual;
	private NodoSencillo<T> ultimo;
	
	public ListaEncadenada() {
		cabeza = null;
		actual = cabeza;
		size = 0;
	}

	public Iterator<T> iterator() {

		return new Iterator<T>() {
			NodoSencillo<T> actual = cabeza;

			@Override
			public boolean hasNext() {return actual!=null;

				

			}

			@Override
			public T next() {T item = actual.darItem();
			actual = actual.darSiguiente();
			return item;}

		};

	}

	@Override
	public void agregarElementoFinal(T elem) {
		
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
		if (cabeza == null) {
			cabeza = nuevo;
			actual = cabeza;
			ultimo = cabeza;
			size++;
		} else {
			NodoSencillo<T> a = ultimo;
			ultimo = nuevo;
			a.cambiarSiguiente(ultimo);
			size++;
		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if (pos == 0) {
			return cabeza.darItem();
		}
		int contador = 0;
		NodoSencillo<T> actual = cabeza;
		while (actual.darSiguiente() != null) {
			actual = actual.darSiguiente();
			contador++;

			if (contador == pos) {
				return actual.darItem();
			}
		}
		return null;

	}

	@Override
	public int darNumeroElementos() {

		return size;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		if (actual == null)
			return null;

		else
			return actual.darItem();

	}

	public boolean avanzarSiguientePosicion() {

		if (actual != null) {
			if (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
				return true;
			}
		}

		return false;
	}

	public boolean retrocederPosicionAnterior() {

		NodoSencillo<T> anterior = null;
		NodoSencillo<T> act2 = cabeza;
		if (act2 != null) {
			while (act2.darSiguiente() != null && !act2.equals(actual)) {
				anterior = act2;
				act2 = act2.darSiguiente();
			}
			if (act2.equals(actual)) {
				actual = anterior;
				return true;
			}

		}
		return false;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		NodoSencillo<T> anterior = null;
		NodoSencillo<T> actual = cabeza;
		int contador =0;
		while (actual!= null)
		{
			
			if(contador==pos&&actual==cabeza)
			{
				NodoSencillo<T> aEliminar = cabeza;
				if(cabeza.darSiguiente()==null)
				cabeza = null;
				else
				{
					cabeza = cabeza.darSiguiente();
				}
				size--;
				return (T) aEliminar.darItem();
				
			}
			else if(contador==pos)
			{
				NodoSencillo<T> aEliminar2 = actual;
				anterior.cambiarSiguiente(actual.darSiguiente());
				size--;
		        return (T)aEliminar2.darItem();
		    }
			
			anterior = actual;
			actual = actual.darSiguiente();
			contador++;
		}
		return null;
	}

	
	public void cambiarElemento(int i, int j) {
		NodoSencillo<T> nodoI = cabeza;
		NodoSencillo<T> nodoJ = cabeza;

		while(i>0 || j>0){
			if(i>0){
				nodoI=nodoI.darSiguiente();
				i--;
			}
			if(j>0){
				nodoJ=nodoJ.darSiguiente();
				j--;
			}
		}

		T aux = nodoJ.darItem();
		nodoJ.cambiarItem(nodoI.darItem());
		nodoI.cambiarItem(aux);
	}
	
	public void agregarAlPrincipio(T item)
	{
		NodoSencillo<T> nuevoNodo = new NodoSencillo<T> (item);
		if(size==0) cabeza = nuevoNodo;
		else
		{
			nuevoNodo.cambiarSiguiente(cabeza);
			cabeza = nuevoNodo;
		}
		size++;
	}
	public boolean contieneElemento(T elem)
	{
		boolean existe=false;
		 
        NodoSencillo<T> aux=cabeza;
 
        while(aux!=null && !existe){
 
            if(aux.darItem().equals(elem)){
                existe=true;
                return existe;
            }
             
            aux=aux.darSiguiente();
        }
 
        return existe;
	}
}
