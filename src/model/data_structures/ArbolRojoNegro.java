package model.data_structures;

import java.util.NoSuchElementException;

import model.vo.VOPelicula;

public class ArbolRojoNegro <Key extends Comparable<Key>, Value>  {
	private static final boolean RED = true;
	private static final boolean BLACK = false;
	private NodoRojoNegro<Key, Value> root;
	private ListaEncadenada<Value> inor;

	public void put(Key key, Value val)
	{ 
		root = put(root, key, val);
		root.color = BLACK;
	}

	public NodoRojoNegro<Key, Value> put(NodoRojoNegro<Key, Value> h, Key key, Value val)
	{
		if (h == null) 
			return new NodoRojoNegro<Key, Value>(key, val, 1, RED);
		else
		{
		int cmp = key.compareTo((Key) h.key);
		
		if (cmp < 0) 
		h.left = put(h.left, key, val);
		
		else if (cmp > 0) 
		h.right = put(h.right, key, val);
		
		else h.val = val;
		
		//////////////////////
		//System.out.println(h.color);
		if (isRed(h.right) && !isRed(h.left)) 
		{
			//System.out.println("opcion 1");
			//h = rotateLeft(h);
		}	
		if (isRed(h.left) && isRed(h.left.left)) 
		{
			//System.out.println("opcion 2");
			h = rotateRight(h);
		}
		if (isRed(h.left) && isRed(h.right)) 
		{  //System.out.println("opcion 3");
			flipColors(h);
		}
			
		h.N = size(h.left) + size(h.right) + 1;
		return h;
		}
	}

	public void flipColors(NodoRojoNegro<Key, Value> x) {
		if(x.color==RED)
		{
			x.color=BLACK;
			return;
		}
		x.color=RED;
	}

	public int size()
	{
		return size(root); 
	}
	private int size(NodoRojoNegro<Key, Value> x)
	{
		if (x == null) return 0;
		else return x.N;
	}

	public NodoRojoNegro<Key, Value> rotateLeft(NodoRojoNegro<Key, Value> h) {
		   
		        NodoRojoNegro<Key, Value> x = h.right;
		        if(x==null)
		        
		        h.right = null;
		        
		        else
		        h.right = x.left;
				//System.out.println("Prueba x");
				x.left = h;
				x.color = h.color;
				h.color = RED;
				x.N = h.N;
				h.N = 1 + size(h.left)
						+ size(h.right);
				return x;
		}

	public NodoRojoNegro<Key, Value> rotateRight(NodoRojoNegro<Key, Value> h)
	{
		//System.out.println("Entr�");
		NodoRojoNegro<Key, Value> x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)
				+ size(h.right);
		return x;
	}

	public boolean isRed(NodoRojoNegro<Key, Value> x) {
		if(x!=null)
			return x.color==RED;

		return false;
	}
	
	
		public Value get(Key key) {
			return get(root, key);
		}

		private Value get(NodoRojoNegro<Key, Value> x, Key key) {
			if (x == null)
				return null;
			int cmp = key.compareTo((Key) x.key);
			if (cmp < 0)
				return get(x.left, key);
			else if (cmp > 0)
				return get(x.right, key);
			else
				return (Value) x.val;
		}
		
		public NodoRojoNegro<Key, Value> getNodo(Key key) {
			return getNodo(root, key);
		}

		public NodoRojoNegro<Key, Value> getNodo(NodoRojoNegro<Key, Value> x, Key key) {
			if (x == null)
				return null;
			int cmp = key.compareTo((Key) x.key);
			if (cmp < 0)
				return getNodo(x.left, key);
			else if (cmp > 0)
				return getNodo(x.right, key);
			else
				return x;
		}
		
		public boolean isEmpty()
		{
			return root==null;
		}
	 
		public boolean contains(Key key)
		{
				return getNodo(key)==null;
		}
		
		 public Key min() 
		 {if (isEmpty()) throw new NoSuchElementException("El arbol esta vacio");
		   return min(root).key;
		 }
		 public NodoRojoNegro<Key, Value> min(NodoRojoNegro<Key, Value> x) 
		 { if (x.left == null) return x; 
		   else return min(x.left); 
		 } 
		 
		 public Key max() 
		 {
			 if (isEmpty()) throw new NoSuchElementException("No se puede obtener la llave maxima porque el arbol esta vacio");
		     return max(root).key;
		 } 

		 public NodoRojoNegro<Key,Value> max(NodoRojoNegro<Key, Value> x) 
		 {   if (x.right == null) return x; 
		     else   return max(x.right); 
		 } 
		 
		 public void deleteMin() {
		        if (isEmpty()) throw new NoSuchElementException("No se puede eliminar ningun elemento, el arbol esta vacio");

		        
		        if (!isRed(root.left) && !isRed(root.right))
		            root.color = RED;

		        root = deleteMin(root);
		        if (!isEmpty()) root.color = BLACK;
		      
		    }

		    
		    public NodoRojoNegro<Key, Value> deleteMin(NodoRojoNegro<Key, Value> h) 
		    { 
		        if (h.left == null)
		            return null;

		        if (!isRed(h.left) && !isRed(h.left.left))
		            h = moveRedLeft(h);

		        h.left = deleteMin(h.left);
		        return balance(h);
		    }
		    
		    public void deleteMax() {
		        if (isEmpty()) throw new NoSuchElementException("No se puede eliminar ningun elemento, el arbol esta vacio");

		    
		        if (!isRed(root.left) && !isRed(root.right))
		            root.color = RED;

		        root = deleteMax(root);
		        if (!isEmpty()) root.color = BLACK;
		        
		    }

		    public NodoRojoNegro<Key, Value> deleteMax(NodoRojoNegro<Key, Value> h) { 
		        if (isRed(h.left))
		            h = rotateRight(h);

		        if (h.right == null)
		            return null;

		        if (!isRed(h.right) && !isRed(h.right.left))
		            h = moveRedRight(h);

		        h.right = deleteMax(h.right);

		        return balance(h);
		    }

		    public void delete(Key key) { 
		        if (key == null) throw new IllegalArgumentException("El arbol esta vacio, no se puede eliminar ningun elemento");
		        if (!contains(key)) return;

		      
		        if (!isRed(root.left) && !isRed(root.right))
		            root.color = RED;

		        root = delete(root, key);
		        if (!isEmpty()) root.color = BLACK;
		       
		    }

		    
		    public NodoRojoNegro<Key, Value> delete(NodoRojoNegro<Key, Value> h, Key key) 
		    {  if (key.compareTo(h.key) < 0)  
		        {
			       if (!isRed(h.left) && !isRed(h.left.left))
				   h = moveRedLeft(h);
			       h.left = delete(h.left, key);
		        }
		        else {
		            if (isRed(h.left))
		                h = rotateRight(h);
		            if (key.compareTo(h.key) == 0 && (h.right == null))
		                return null;
		            if (!isRed(h.right) && !isRed(h.right.left))
		                h = moveRedRight(h);
		            if (key.compareTo(h.key) == 0) {
		                NodoRojoNegro<Key, Value> x = min(h.right);
		                h.key = x.key;
		                h.val = x.val;
		                h.right = deleteMin(h.right);
		            }
		            else h.right = delete(h.right, key);
		        }
		        return balance(h);
		    }
		    
		  public NodoRojoNegro<Key, Value> moveRedLeft(NodoRojoNegro<Key, Value> h) 
		  { flipColors(h);
		    if (isRed(h.right.left)) 
		    { 
		    h.right = rotateRight(h.right);
		    h = rotateLeft(h);
		    flipColors(h);
		    }
		    return h;
		    }

		  public NodoRojoNegro<Key, Value> moveRedRight(NodoRojoNegro<Key, Value> h ) 
		  {
		    flipColors(h);
		   if (isRed(h.left.left)) 
		   { 
		   h = rotateRight(h);
		   flipColors(h);
		   }
		    return h;
		  }

		  
		  public NodoRojoNegro<Key, Value> balance(NodoRojoNegro<Key, Value> h)
		  {  if (isRed(h.right))                      h = rotateLeft(h);
		     if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		     if (isRed(h.left) && isRed(h.right))     flipColors(h);
		     h.N = size(h.left) + size(h.right) + 1;
		     return h;
		  }

		  public Iterable<Key> keys() {
				if (isEmpty()) return (Iterable<Key>) new Queue<Key>();
				return keys(min(), max());
			}
		  
		  private void keys(NodoRojoNegro<Key,Value> x, Queue<Key> queue, Key lo, Key hi) { 
				if (x == null) return; 
				int cmplo = lo.compareTo( x.key); 
				int cmphi = hi.compareTo( x.key); 
				if (cmplo < 0) keys(x.left, queue, lo, hi); 
				if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
				if (cmphi > 0) keys(x.right, queue, lo, hi); 
			} 
		  public Iterable<Key> keys(Key lo, Key hi) {
				if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
				if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");
                Queue<Key> queue = new Queue<Key>();
				keys(root, queue, lo, hi);
				return (Iterable<Key>) queue;
			}

		public NodoRojoNegro getRoot() {
			// TODO Auto-generated method stub
			return root;
		} 
		public ListaEncadenada inorden(NodoRojoNegro pRoot)
		{ inor = new ListaEncadenada();
			if(pRoot!=null)
			{
			 aux(pRoot);	
			}
			return inor;
		}
		private void aux(NodoRojoNegro nodo)
		{
			if(nodo.getNodeLeft()!=null)
			{
				aux(nodo.getNodeLeft());
			}
			Value peli = (Value) nodo.val;
			inor.agregarAlPrincipio(peli);
			if(nodo.getNodeRight()!=null)
			{
				aux(nodo.getNodeRight());
			}
		}
		
		public int rank(Key key) {
	        if (key == null) throw new IllegalArgumentException("argument to rank() is null");
	        return rank(key, root);
	    } 

	    // number of keys less than key in the subtree rooted at x
	    private int rank(Key key, NodoRojoNegro x) {
	        if (x == null) return 0; 
	        int cmp = key.compareTo((Key) x.key); 
	        if      (cmp < 0) return rank(key, x.left); 
	        else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
	        else              return size(x.left); 
	    } 

}
