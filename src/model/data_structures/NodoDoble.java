package model.data_structures;

public class NodoDoble<E> {
	
	private E item;
	private NodoDoble<E> next;
	private NodoDoble<E> previous;
	
	public NodoDoble ( E item )
	{
		this.item = item;
		next = null;
		previous = null;
	}
	
	public NodoDoble<E> getNext()
	{
		return next;
	}
	
	public NodoDoble<E> getPrevious()
	{
		return previous;
	}
	
	public void setNext( NodoDoble<E> next )
	{
		this.next = next;
	}
	
	public void setPrevious( NodoDoble<E> previous )
	{
		this.previous = previous;
	}
	
	public void setItem( E item )
	{
		this.item = item;
	}
	
	public E getItem()
	{
		return item;
	}
	
}
