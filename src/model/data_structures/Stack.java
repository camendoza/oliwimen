package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements ILista {
	NodoSencillo<T> ult;
	NodoSencillo<T> act;
	int size;
	ListaEncadenada<T> lista;

	Stack() {
		lista = new ListaEncadenada<T>();
		size = 0;
		ult = null;
		act = ult;
	}

	public void push(T item) {
		NodoSencillo<T> nuevo = new NodoSencillo<T>(item);
		if (ult == null) {
			ult = nuevo;
		} else {
			nuevo.cambiarSiguiente(ult);
			ult = nuevo;
		}

	}

	public T pop() {
		if (ult != null) {
			NodoSencillo<T> temp = ult.darSiguiente();
			ult = null;
			ult = temp;
		}
		size--;
		return ult.darItem();

	}

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		}
		return false;
	}

	@Override
	public Iterator iterator() {
		return new Iterator<T>() {
			NodoSencillo<T> actual = null;

			@Override
			public boolean hasNext() {
				if (size == 0)
					return false;

				if (actual == null)
					return true;

				return actual.darSiguiente() != null;
			}

			@Override
			public T next() {
				if (actual != null)
					actual = ult;

				else
					actual = actual.darSiguiente();

				return actual.darItem();
			}

		};
	}

	@Override
	public void agregarElementoFinal(Object elem) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object darElemento(int pos) {
		if (pos == 0) {
			return ult.darItem();
		}
		int contador = 0;
		NodoSencillo<T> actual = ult;
		while (actual.darSiguiente() != null) {
			actual = actual.darSiguiente();
			contador++;

			if (contador == pos) {
				return actual.darItem();
			}
		}
		return null;
	}

	@Override
	public int darNumeroElementos() {

		return size;
	}

	@Override
	public Object darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return act.darItem();

	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub

		if (act != null) {
			if (act.darSiguiente() != null) {
				act = act.darSiguiente();
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		
		
		
		
		return false;
		
}

	@Override
	public Object eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cambiarElemento(int j, int i) {
		// TODO Auto-generated method stub
		
	}
}
