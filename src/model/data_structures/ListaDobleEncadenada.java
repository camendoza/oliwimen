package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> first;
	private NodoDoble<T> last;
	private int size;
	private NodoDoble<T> actual;

	public ListaDobleEncadenada() {
		first = null;
		last = null;
		size = 0;
		actual = null;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			NodoDoble<T> actual = null;

			@Override
			public boolean hasNext() {
				if (size == 0)
					return false;

				if (actual == null)
					return true;

				return actual.getNext() != null;
			}

			@Override
			public T next() {
				if (actual != null)
					actual = first;

				else
					actual = actual.getNext();

				return actual.getItem();
			}

		};

	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nElem = new NodoDoble<T>(elem);
		if (first == null) {
			first = nElem;
			last = nElem;
			actual = nElem;
			size++;
		} else {
			nElem.setPrevious(last);
			last.setNext(nElem);
			last = nElem;
			size++;
		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if (pos == 0) {
			return first.getItem();
		}
		int contador = 0;
		NodoDoble<T> actual = first;
		while (actual.getNext() != null) {
			actual = actual.getNext();
			contador++;

			if (contador == pos) {
				return actual.getItem();
			}
		}
		return null;
	}

	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		if (actual == null)
			return null;
		else
			return actual.getItem();
	}

	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if (actual != null) {
			if (actual.getNext() != null) {
				actual = actual.getNext();
				return true;
			}
		}

		return false;

	}

	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if (actual == null)
			return false;
		else {
			if (actual.getPrevious() != null) {
				actual = actual.getPrevious();
				return true;
			}
		}
		return false;

	}

	@Override
	public T eliminarElemento(int pos) {
		int contador =0;
		Iterator<T> i = iterator();
		while(i.hasNext()&&contador<pos)
		{
			
			if(contador==pos)
			{ System.out.println("Pruebaaaaaaa 2");
				NodoDoble<T> aEliminar;
				if(actual == first)
				{
					aEliminar = first;
					if(first.getNext()==null)
					{
						
						first = null;
					}
					else
					{
						NodoDoble<T> siguiente = first.getNext();
						siguiente.setPrevious(null);
						first = siguiente;
					}
					size--;
					
					return (T) aEliminar.getItem();
				}
				else
				{
					System.out.println("Pruebaaaaaaa 3");
					NodoDoble<T> aEliminar2 = actual;
					NodoDoble<T> anterior = actual.getPrevious();
					NodoDoble<T> siguiente = actual.getNext();
					if(actual.getNext()==null)
					{
						anterior.setNext(siguiente);
						siguiente.setPrevious(anterior);
						size--;
						
						return (T) aEliminar2.getItem();
					}
					
				}
				
			}
			else
			{
				System.out.println("Avanzo");
				avanzarSiguientePosicion();
				contador++;
				System.out.println(contador);
			}
		}
		
		
		
		return null;
	}


	public void cambiarElemento(int i, int j) {
		NodoDoble<T> nodoI = first;
		NodoDoble<T> nodoJ = first;

		while(i>0 || j>0){
			if(i>0){
				nodoI=nodoI.getNext();
				i--;
			}
			if(j>0){
				nodoJ=nodoJ.getNext();
				j--;
			}
		}

		T aux = nodoJ.getItem();
		nodoJ.setItem(nodoI.getItem());
		nodoI.setItem(aux);
	}

}
