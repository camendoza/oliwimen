package model.vo;

import model.data_structures.ListaEncadenada;

public class VOGeneroRatings
{
	private String genero;
	private ListaEncadenada<VORating> ratings;
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public ListaEncadenada<VORating> getRatings() {
		return ratings;
	}
	public void setRatings(ListaEncadenada<VORating> ratings) {
		this.ratings = ratings;
	}
}
