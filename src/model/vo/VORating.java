package model.vo;

public class VORating {
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private double timestap;
	private double error;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public double getTimestap()
	{
		return timestap;
	}
	public void setTimestap(double timestap)
	{
		this.timestap=timestap;
	}
	public double getError() {
		
		return error;
	}
	public void setError(double error)
	{
		this.error = error;
	}
	

}
