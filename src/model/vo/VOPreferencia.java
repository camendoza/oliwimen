package model.vo;

public class VOPreferencia implements Comparable<VOPreferencia> {
	
	String pelicula;
	Integer agno;
	
	
	public void setPelicula(String pPelicula)
	{
		pelicula = pPelicula;
	}
	
	public void setAgno(Integer pAgno)
	{
		agno = pAgno;
	}
	
	public String getPelicula()
	{
		return pelicula;
	}
	
	public Integer getAgno()
	{
		return agno;
	}

	@Override
	public int compareTo(VOPreferencia o) {
		int aux = agno-o.getAgno();
		if (aux==0)
			return pelicula.compareTo(o.getPelicula());
		return aux;
	}

}
