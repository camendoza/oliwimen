package model.vo;

import model.data_structures.ILista;

/**
 * @author Venegas
 *
 */

public class VOUsuarioPelicula {
	
	/**
	 * nombre de la pel�cula
	 */

	private String nombrepelicula;
	private long idPelicula;
	
	/**
	 * rating dado por el usuario
	 */
	private double ratingUsuario;
	
	/**
	 * rating calculado por el sistema
	 */

	private double ratingSistema;
	
	/**
	 * error sobre el rating
	 */
	
	private double errorRating;
	
	/**
	 * list de tags generados por el usuario sobre la pelicula
	 */
	
	private ILista<VOTag> tags;
	
	/**
	 * id del usuario
	 */
	
	private Integer idUsuario;

	public VOUsuarioPelicula() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNombrepelicula() {
		return nombrepelicula;
	}

	public void setNombrepelicula(String nombrepelicula) {
		this.nombrepelicula = nombrepelicula;
	}

	public double getRatingUsuario() {
		return ratingUsuario;
	}

	public void setRatingUsuario(double ratingUsuario) {
		this.ratingUsuario = ratingUsuario;
	}

	public double getRatingSistema() {
		return ratingSistema;
	}

	public void setRatingSistema(double ratingSistema) {
		this.ratingSistema = ratingSistema;
	}

	public double getErrorRating() {
		return errorRating;
	}

	public void setErrorRating(double errorRating) {
		this.errorRating = errorRating;
	}

	public ILista<VOTag> getTags() {
		return tags;
	}

	public void setTags(ILista<VOTag> tags) {
		this.tags = tags;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public long getIdPelicula() {
		return idPelicula;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
}
