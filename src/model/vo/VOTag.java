package model.vo;

public class VOTag {
	
	/**
	 * contenido del tag
	 */
	private String contenido;
	private long idMovie;
	private long idUsuario;
	private long timestamp;
	
	public VOTag() {
		// TODO Auto-generated constructor stub
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public long getIdMovie() {
		return idMovie;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdMovie(long idMovie)
	{
		this.idMovie = idMovie;
	}
	public void setIdUsuario(long idMovie)
	{
		this.idMovie = idMovie;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
