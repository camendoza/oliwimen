package model.vo;

import java.util.Date;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.SeparateChainingHashST;

public class VOPelicula {
	/*
	 * nombre de la pel�cula
	 */
	private String nombre;

	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	private Date fechaLanzamineto;

	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	private ILista<VOGeneroPelicula> generosAsociados;

	/*
	 * votos totales sobre la pel�cula
	 */
	private int votostotales;  

	/*
	 * promedio anual de votos (hasta el 2016)
	 */
	private int promedioAnualVotos; 

	/*
	 * promedio IMBD
	 */
	
	
	private double ratingIMBD;

	private long movieId;

	private String director;

	private String[] actores;
	
	private ListaEncadenada<String> paises;
	
	private Integer anio;
	
	private int cantidadRatings;


	public VOPelicula() {
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaLanzamiento() {
		return fechaLanzamineto;
	}

	public void setFechaLanzamiento(Date fechaLanzamineto) {
		this.fechaLanzamineto = fechaLanzamineto;
	}

	public ILista<VOGeneroPelicula> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ILista<VOGeneroPelicula> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public int getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(int promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}

	public long getMovieId()
	{
		return movieId;
	}

	public void setDirector(String director) {
		this.director=director;
	}

	public String getDirector()
	{
		return director;
	}

	public void setActores(String[] actores) {
		this.actores = actores;
	}

	public String[] getActores()
	{
		return actores;
	}

	public ListaEncadenada  getPaises() {
		return paises;
	}
	public void setPais(ListaEncadenada paises) {
		this.paises = paises;
	}

	public void setAnio(Integer year) {
		// TODO Auto-generated method stub
		this.anio = year; 
	}
	public Integer getAnio() {
		// TODO Auto-generated method stub
		return anio; 
	}

	public int getCantidadRatings() {
		return cantidadRatings;
	}

	public void setCantidadRatings(int cantidadRatings) {
		this.cantidadRatings = cantidadRatings;
	}
	
	

}
