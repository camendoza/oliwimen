package model.vo;

import model.data_structures.ListaEncadenada;

public class VOGeneroPelicula 
{
	private String genero;

	private ListaEncadenada<VOPelicula> peliculas;

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public ListaEncadenada<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ListaEncadenada<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	} 
}
