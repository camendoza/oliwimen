package model.vo;

import model.data_structures.ListaEncadenada;

public class VOUsuarioSegmento
{
	private Long idUsuario;
	private ListaEncadenada<VORating> ratings;
	// ------------------------------------------------
	public ListaEncadenada<VORating> getRatings() {
		return ratings;
	}
	public void setRatings(ListaEncadenada<VORating> ratings) {
		this.ratings = ratings;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
}
