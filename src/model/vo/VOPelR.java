package model.vo;

public class VOPelR {
	private Integer idUsuario;
	private Long id1;
	private Long id2;
	private Double rating1;
	private Double rating2;

	public Integer getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public Long getIdPel1() {
		return id1;
	}
	
	public void setIdPel1(Long id) {
		this.id1 = id;
	}
	
	public Long getIdPel2() {
		return id2;
	}
	
	public void setIdPel2(Long idPel2) {
		this.id2 = idPel2;
	}
	
	public Double getRatingP1() {
		return rating1;
	}
	
	public void setRatingP1(Double ratingP1) {
		this.rating1 = ratingP1;
	}
	
	public Double getRatingP2() {
		return rating2;
	}

	public void setRatingP2(Double ratingP2) {
		this.rating2 = ratingP2;
	}
}
