package model.vo;

public class VOPrioridadPelicula implements Comparable<VOPrioridadPelicula>
{
	private VOPelicula pelicula;
	
	public VOPrioridadPelicula(VOPelicula pelicula)
	{
		this.pelicula = pelicula;
	}
	public VOPelicula getPelicula()
	{
		return pelicula;
	}

	@Override
	public int compareTo(VOPrioridadPelicula that) {
		double auxThis = pelicula.getPromedioAnualVotos()*pelicula.getRatingIMBD();
		double auxThat = that.getPelicula().getPromedioAnualVotos()*that.getPelicula().getRatingIMBD();
		return Double.compare(auxThis, auxThat);
	}
}
